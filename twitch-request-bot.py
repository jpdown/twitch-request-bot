#Song request bot for Twitch, saves song requests to text files
#Coded by PoisonedPanther
#GPLv3 License, see github repo https://github.com/PoisonedPanther/twitch-request-bot

import configparser
import os.path
import asyncio
from collections import deque
import json
import aiohttp
import async_timeout

class TwitchListener:

    async def twitchConnect(self, host, loop):
        """Connects to Twitch chat using given config values"""
        print("[Twitch] Connecting")
        self.reader, self.writer = await asyncio.open_connection(host, 443, ssl=True, loop=loop) #open ssl connection to twitch IRC server
        await asyncio.sleep(3) #give it enough time to open the connection
        #send password and username TODO: allow justinfan users
        self.writer.write(b"PASS " + config['Twitch']['oauth-token'].encode("utf-8") + b"\r\n")
        self.writer.write(b"NICK " + config['Twitch']['username'].lower().encode("utf-8") + b"\r\n")
        self.writer.write(b"CAP REQ :twitch.tv/tags\r\n") #request tags to see who is mod, sub, etc.
        self.writer.write(b"CAP REQ :twitch.tv/commands\r\n") #pretty much just for RECONNECT
        self.writer.write(b"JOIN #" + config['Twitch']['channel'].lower().encode("utf-8") + b"\r\n") #join given channel
        #assign some variables for later use
        self.normalRequestsEnabled = True
        self.prioRequestsEnabled = True
        #load json file for currency data
        if os.path.exists("currency.json"):
            with open("currency.json", "r") as currencyData:
                self.currencyDicts = json.load(currencyData)
        else:
            self.currencyDicts = [{},{}]
        #load json file for request data
        if os.path.exists("requests.json") and config.getboolean("Commands", "save-requests-for-next-launch"):
            with open("requests.json", "r") as requestsJson:
                self.Requests = json.load(requestsJson)
        else:
            self.Requests = [[],[],[],[]] #normal requests, normal requesters, prio requests, prio requesters

    async def ircListener(self, loop):
        """Listens to IRC messages and parses them"""
        while True:
            try:
                messageData = (await self.reader.readuntil(b"\n")).decode("utf-8") #wait for message
            except asyncio.streams.IncompleteReadError: #server disconnect
                break
            messageData = messageData.rstrip().split()
            if messageData[0].startswith("@"): #if message starts with @ (chat message with tags)
                #make dictionary with each tag
                tags = {}
                allTags = messageData.pop(0).lstrip("@").split(";")
                for i in allTags:
                    tag = i.split("=")
                    tags[tag[0]] = tag[1]
            else:
                tags = None
            if messageData[0] == "PING":
                self.writer.write(b"PONG %s\r\n" % messageData[1].encode("utf-8")) #ensure connection isn't terminated by IRC server
            elif messageData[1] == "RECONNECT":
                print("Twitch has requested I reconnect, I do not support that currently. Please restart the bot.") #for now
                #TODO: code reconnect functionality
            elif messageData[1] == "JOIN": #if joined channel, output
                print("[Twitch] Joined Channel: " + messageData[2])
            elif messageData[1] == "PRIVMSG": #if chat message
                sender = tags["display-name"] if tags["display-name"] != "" else messageData[0].lstrip(":").split("!")[0] #use display name if possible
                message = " ".join(messageData[3:]).lstrip(":").split()
                #determine userType (broadcaster,mod,sub,normal)
                if "broadcaster" in tags["badges"]:
                    userType = "broadcaster"
                elif tags["mod"] == "1":
                    userType = "mod"
                elif tags["subscriber"] == "1":
                    userType = "sub"
                else:
                    userType = "normal"
                if "bits" in tags:
                    if config.getboolean('Currency', 'enabled') and config.getboolean('Currency', 'bits-auto-give-tokens'): #if currency enabled and bits auto give tokens
                        accountStatus, userID = await self.currencyAccountChecking(sender, sender, tags["user-id"])
                        value = int(int(tags["bits"]) / config.getint('Currency', 'bits-per-token')) #get number of tokens from bits and truncate decimal
                        if value != 0 and accountStatus: #if value isn't 0 and account valid
                            await self.currencyAccountManagement(userID, "add", value)
                            print("{0} awarded {1} {2} for {3} bits".format(sender, value, config['Currency']['currency-name'], tags["bits"]))
                await self.commandParsing(message, sender, tags, userType)
            elif messageData[1] == "USERNOTICE": #if sub or resub (or raid)
                if config.getboolean('Currency', 'enabled') and config.getboolean('Currency', 'subs-auto-give-tokens'): #if currency enabled and subs auto give tokens
                    if "sub" in tags["msg-id"]: #if message is sub or resub
                        if config['Currency']['sub-gift-tokens'] == "split": #if splitting tokens
                            if tags["msg-param-sub-plan"] == "1000" and 0 <= config.getint('Currency', 'sub-gift-tier1-receiver-split') <= 100:
                                tokens = config.getint('Currency', 'sub-tier1-tokens')
                                tokensReceiver = int(round(tokens * (config.getint('Currency', 'sub-gift-tier1-receiver-split') / 100)))
                                tokensGifter = tokens - tokensReceiver
                                print("{0} awarded {2} {4} and {1} awarded {3} {4} for tier 1 sub gift".format(tags["display-name"], tags["msg-param-recipient-display-name"], tokensGifter, tokensReceiver, config['Currency']['currency-name']))
                            elif tags["msg-param-sub-plan"] == "2000" and 0 <= config.getint('Currency', 'sub-gift-tier2-receiver-split') <= 100:
                                tokens = config.getint('Currency', 'sub-tier2-tokens')
                                tokensReceiver = int(round(tokens * (config.getint('Currency', 'sub-gift-tier2-receiver-split') / 100)))
                                tokensGifter = tokens - tokensReceiver
                                print("{0} awarded {2} {4} and {1} awarded {3} {4} for tier 2 sub gift".format(tags["display-name"], tags["msg-param-recipient-display-name"], tokensGifter, tokensReceiver, config['Currency']['currency-name']))
                            elif tags["msg-param-sub-plan"] == "3000" and 0 <= config.getint('Currency', 'sub-gift-tier3-receiver-split') <= 100:
                                tokens = config.getint('Currency', 'sub-tier3-tokens')
                                tokensReceiver = int(round(tokens * (config.getint('Currency', 'sub-gift-tier3-receiver-split') / 100)))
                                tokensGifter = tokens - tokensReceiver
                                print("{0} awarded {2} {4} and {1} awarded {3} {4} for tier 3 sub gift".format(tags["display-name"], tags["msg-param-recipient-display-name"], tokensGifter, tokensReceiver, config['Currency']['currency-name']))
                            else:
                                print("Invalid sub gift split percentage given, please fix in the config")
                                await self.sendChatMessage("@{0} Invalid sub gift split percentage given, please fix in the config".format(config['Twitch']['Channel']))
                                continue
                            accountStatus, userID = await self.currencyAccountChecking(tags["login"], tags["login"], tags["user-id"]) #check account status of gifter
                            if accountStatus: #if account valid
                                await self.currencyAccountManagement(userID, "add", tokensGifter)
                            accountStatus, userID = await self.currencyAccountChecking(tags["msg-param-recipient-user-name"], tags["msg-param-recipient-user-name"], tags["msg-param-recipient-id"]) #check account status of gifter
                            if accountStatus: #if account valid
                                await self.currencyAccountManagement(userID, "add", tokensReceiver)
                        else: #if not splitting tokens
                            if tags["msg-id"] == "subgift" and config['Currency']['sub-gift-tokens'] == "receiver": #if sub gift and receiver of gift gets tokens
                                displayName, userID = tags["msg-param-recipient-display-name"], tags["msg-param-recipient-id"]
                            else: #if not sub gift or sender receives managetokens
                                displayName, userID = tags["display-name"], tags["user-id"]
                            accountStatus, userID = await self.currencyAccountChecking(displayName.lower(), displayName.lower(), userID) #check account status
                            if accountStatus: #if account valid
                                if tags["msg-param-sub-plan"] == "Prime":
                                    await self.currencyAccountManagement(userID, "add", config.getint('Currency', 'sub-prime-tokens'))
                                    print("{0} awarded {1} {2} for prime sub".format(displayName, config['Currency']['sub-prime-tokens'], config['Currency']['currency-name']))
                                elif tags["msg-param-sub-plan"] == "1000":
                                    await self.currencyAccountManagement(userID, "add", config.getint('Currency', 'sub-tier1-tokens'))
                                    print("{0} awarded {1} {2} for tier 1 sub".format(displayName, config['Currency']['sub-tier1-tokens'], config['Currency']['currency-name']))
                                elif tags["msg-param-sub-plan"] == "2000":
                                    await self.currencyAccountManagement(userID, "add", config.getint('Currency', 'sub-tier2-tokens'))
                                    print("{0} awarded {1} {2} for tier 2 sub".format(displayName, config['Currency']['sub-tier2-tokens'], config['Currency']['currency-name']))
                                elif tags["msg-param-sub-plan"] == "3000":
                                    await self.currencyAccountManagement(userID, "add", config.getint('Currency', 'sub-tier3-tokens'))
                                    print("{0} awarded {1} {2} for tier 3 sub".format(displayName, config['Currency']['sub-tier3-tokens'], config['Currency']['currency-name']))

    async def sendChatMessage(self, message):
        """Sends given messsage to Twitch chat channel"""
        self.writer.write("PRIVMSG #{0} :".format(config['Twitch']['channel'].lower()).encode("utf-8") + message.encode("utf-8") + b"\r\n")

    async def updateRequestList(self, update="both"):
        """Updates request list text files"""
        if update == "prio" or update == "both":
            with open("priorequests.txt", "w") as file: #write first line
                file.write(config['Text']['prio-request-text'])
            with open("priorequests.txt", "a") as file:
                if len(self.Requests[2]) >= config.getint('Commands', 'max-prio-requests') and config.getint('Commands', 'max-prio-requests') != -1 and config.getboolean("Text", "write-limit-reached-to-file"): #if limit reached and not unlimited and writing limit reached is true
                    file.write(" (LIMIT REACHED)")
                if config.getboolean("Text", "write-request-status-to-file"): #if writing requests OPEN or closed
                    file.write(" (OPEN)" if self.prioRequestsEnabled else " (CLOSED)")
                for r, n in zip(self.Requests[2], self.Requests[3]): #for each request, write new line
                    file.write("\n    '{0}' [{1}]".format(r, n))
        if update == "normal" or update == "both":
            with open("normalrequests.txt", "w") as file: #write first line
                file.write(config['Text']['normal-request-text'])
            with open("normalrequests.txt", "a") as file:
                if len(self.Requests[0]) >= config.getint('Commands', 'max-normal-requests') and config.getint('Commands', 'max-normal-requests') != -1 and config.getboolean("Text", "write-limit-reached-to-file"): #if limit reached and not unlimited and writing limit reached is true
                    file.write(" (LIMIT REACHED)")
                if config.getboolean("Text", "write-request-status-to-file"): #if writing requests OPEN or closed
                    file.write(" (OPEN)" if self.normalRequestsEnabled else " (CLOSED)")
                for r, n in zip(self.Requests[0], self.Requests[1]): #for each request, write new line
                    file.write("\n    '{0}' [{1}]".format(r, n))
        with open("normalrequests.txt", "r") as normal, open("priorequests.txt", "r") as prio, open("combinedrequests.txt", "w") as combined:
            combined.write(prio.read() + "\n" + normal.read()) #write full requests list
        if config.getboolean("Commands", "save-requests-for-next-launch"): #if saving requests
            with open("requests.json", "w") as requestsJson:
                json.dump(self.Requests, requestsJson, indent=2)

    async def permissionCheck(self, command, userType):
        """Checks permissions on given command and returns True or False"""
        permissions = config['Permissions'][command].strip().split(",")
        if permissions.count("everyone") > 0: #if command open to everyone
            return True
        elif permissions.count(userType) > 0: #if user in user defined permission list for command
            return True
        else:
            return False

    async def commandParsing(self, message, sender, tags, userType):
        """Checks if message is command and performs appropriate actions"""
        if message[0] == config['Commands']['Prefix']:
            return #if just prefix, do nothing with the message to avoid possible problems
        elif message[0] == config['Commands']['Prefix'] + config['Commands']['request-command']: #if request command
            if await self.permissionCheck("request", userType): #if allowed to run command
                if len(message) == 1: #if just typed request command with no request
                    await self.sendChatMessage("@{0} Please type the song you want after {1}{2}".format(sender, config['Commands']['Prefix'], config['Commands']['request-command']))
                elif self.normalRequestsEnabled:
                    if self.Requests[1].count(sender) >= config.getint('Commands', 'max-normal-requests-per-user') and config.getint('Commands', 'max-normal-requests-per-user') != -1: #if user request limit reached
                        await self.sendChatMessage("@{0} You've reached your request limit of {1}, please wait until your request(s) have been played".format(sender, config.getint('Commands', 'max-normal-requests-per-user')))
                    elif len(self.Requests[0]) >= config.getint('Commands', 'max-normal-requests') and config.getint('Commands', 'max-normal-requests') != -1: #if general request limit reached
                        await self.sendChatMessage("@{0} Request limit of {1} reached, please wait".format(sender, config['Commands']['max-normal-requests']))
                    else: #if requests are available to be taken
                        request = " ".join(message[1:])
                        if len(" ".join(message[1:])) > config.getint('Commands', 'request-character-limit') and config.getint('Commands', 'request-character-limit') != -1:
                            request = request[:config.getint('Commands', 'request-character-limit')]
                        self.Requests[0].append(request) #add request and sender to deque objects
                        self.Requests[1].append(sender)
                        print("Normal Request: '{0}' requested by @{1}".format(request, sender)) #output in console
                        await self.updateRequestList("normal")
                        await self.sendChatMessage("'{0}' requested successfully @{1}".format(request, sender))
                else: #if requests disabled
                    await self.sendChatMessage("@{0} Normal requests are currently closed".format(sender))
        elif message[0] == config['Commands']['Prefix'] + config['Commands']['prio-request-command']: #if prio request command
            if config.getboolean("Currency", "enabled") or await self.permissionCheck("prio-request-no-currency", userType): #if currency disabled and no permission
                if self.prioRequestsEnabled: #if requests open
                    if len(message) == 1: #if just typed prio request command with no request
                        await self.sendChatMessage("@{0} Please type the song you want after {1}{2}".format(sender, config['Commands']['prefix'], config['Commands']['prio-request-command']))
                    elif len(self.Requests[2]) >= config.getint('Commands', 'max-prio-requests') and config.getint('Commands', 'max-prio-requests') != -1: #if request limit reached
                        await self.sendChatMessage("@{0} Priority request limit of {1} reached, please wait".format(sender, config['Commands']['max-prio-requests']))
                    else: #if requests are available to be taken
                        if config.getboolean("Currency", "enabled"): #if currency enabled
                            accountStatus, userID = await self.currencyAccountChecking(sender, sender, tags["user-id"]) #check account
                            if accountStatus: #if account valid
                                if not await self.currencyAccountManagement(userID, "subtract", 1): #if not able to subtract 1 token
                                    await self.sendChatMessage("@{0} You do not have any {1} to request with".format(sender, config['Currency']['currency-name']))
                                    return
                                elif self.Requests[3].count(sender) >= config.getint('Commands', 'max-prio-requests-per-user') and config['Commands'].getint('max-prio-requests-per-user') != -1: #if user request limit reached
                                    await self.sendChatMessage("@{0} You've reached your prio request limit of {1}, please wait until your request(s) have been played".format(sender, config.getint('Commands', 'max-prio-requests-per-user')))
                                    return
                            else: #if account not valid
                                return
                        request = " ".join(message[1:])
                        if len(" ".join(message[1:])) > config.getint('Commands', 'request-character-limit') and config.getint('Commands', 'request-character-limit') != -1:
                            request = request[:config.getint('Commands', 'request-character-limit')]
                        self.Requests[2].append(request) #add request and sender to deque objects
                        self.Requests[3].append(sender)
                        print("Priority Request: '{0}' requested by @{1}".format(request, sender)) #output in console
                        await self.updateRequestList("prio")
                        if config.getboolean("Currency", "enabled"):
                            await self.sendChatMessage("'{0}' priority requested successfully, you now have {1} {2} @{3}".format(request, self.currencyDicts[1][userID], config['Currency']['currency-name'], sender))
                        else:
                            await self.sendChatMessage("'{0}' priority requested successfully @{1}".format(request, sender))
                else: #if requests disabled
                    await self.sendChatMessage("@{0} Priority requests are currently closed".format(sender))
        elif message[0] == config['Commands']['Prefix'] + config['Commands']['clear-requests-command']: #if clear requests command
            if await self.permissionCheck("clear-requests", userType): #if allowed to run command
                if len(message) == 1: #if specify nothing
                    self.Requests[2].clear() #clear entire queue
                    self.Requests[3].clear()
                    self.Requests[0].clear()
                    self.Requests[1].clear()
                    await self.updateRequestList("both")
                    await self.sendChatMessage("Request queue cleared")
                elif message[1].lower() == "prio": #if specify prio
                    self.Requests[2].clear() #clear prio deque objects
                    self.Requests[3].clear()
                    await self.updateRequestList("prio")
                    await self.sendChatMessage("Priority request queue cleared")
                elif message[1].lower() == "normal": #if specify normal
                    self.Requests[0].clear() #clear normal deque object
                    self.Requests[1].clear()
                    await self.updateRequestList("normal")
                    await self.sendChatMessage("Normal request queue cleared")
                else: #if have argument, but not valid argument
                    await self.sendChatMessage("Please specify 'prio', 'normal' or just put nothing but the command")
        elif message[0] == config['Commands']['Prefix'] + config['Commands']['skip-request-command']: #if skip request command
            if await self.permissionCheck("skip-request", userType): #if allowed to run command
                if len(message) == 1: #if don't specify
                    await self.sendChatMessage("@{0} Please specify 'normal' or 'prio'".format(sender))
                elif message[1].lower() == "prio": #if specify prio
                    if len(self.Requests[2]) > 0: #if there is a prio request
                        del self.Requests[2][0] #skip prio request
                        del self.Requests[3][0]
                        await self.updateRequestList("prio")
                        await self.sendChatMessage("Priority request skipped")
                    else: #if there's not a prio request
                        await self.sendChatMessage("No priority requests to skip")
                elif message[1].lower() == "normal": #if specify normal
                    if len(self.Requests[0]) > 0: #if there is a normal request
                        del self.Requests[0][0] #skip normal request
                        del self.Requests[1][0]
                        await self.updateRequestList("normal")
                        await self.sendChatMessage("Normal request skipped")
                    else: #if there's not a normal request
                        await self.sendChatMessage("No normal requests to skip")
                else: #if have argument but not valid
                    await self.sendChatMessage("@{0} Please specify 'normal' or 'prio'".format(sender))
        elif message[0] == config['Commands']['Prefix'] + config['Commands']['toggle-requests-command']: #if toggle requests command
            if await self.permissionCheck("toggle-requests", userType): #if allowed to run command
                if len(message) == 1: #if prio or normal not specified
                    await self.sendChatMessage("@{0} Please specify 'prio' or 'normal'".format(sender))
                elif message[1] == 'prio': #if specify prio
                    self.prioRequestsEnabled = not self.prioRequestsEnabled #toggle prio requests
                    await self.sendChatMessage("Prio requests are now {0}".format("open" if self.prioRequestsEnabled else "closed"))
                    await self.updateRequestList("prio")
                elif message[1] == 'normal': #if specify prio
                    self.normalRequestsEnabled = not self.normalRequestsEnabled #toggle prio requests
                    await self.sendChatMessage("Normal requests are now {0}".format("open" if self.normalRequestsEnabled else "closed"))
                    await self.updateRequestList("normal")
                else: #if prio or normal not specified
                    await self.sendChatMessage("@{0} Please specify 'prio' or 'normal'".format(sender))
        elif message[0] == config['Commands']['Prefix'] + config['Commands']['remove-request-command']: #if remove request command
            if await self.permissionCheck("remove-request", userType): #if allowed to run command
                if len(message) == 1: #if request not specified
                    await self.sendChatMessage("@{0} please specify 'prio' or 'normal'".format(sender))
                elif message[1].lower() == "prio": #if specified prio
                    try: #try to get value from chat message
                        index = int(message[2]) #get value from message
                    except IndexError: #if can't get int value
                        await self.sendChatMessage("@{0} Please specify the number of the prio request you would like to remove".format(sender))
                        return
                    except ValueError: #if can't get int value
                        await self.sendChatMessage("@{0} Please specify the number of the prio request you would like to remove".format(sender))
                        return
                    try: #try to remove request
                        del self.Requests[2][index - 1]
                        del self.Requests[3][index - 1]
                        await self.updateRequestList("prio")
                        await self.sendChatMessage("Request {0} removed from prio request list".format(index))
                    except IndexError: #if request not in deque
                        await self.sendChatMessage("@{0} Request {1} not in prio request list".format(sender, index))
                elif message[1].lower() == "normal": #if specified normal
                    try: #try to get value from chat message
                        index = int(message[2]) #get value from message
                    except IndexError: #if can't get int value
                        await self.sendChatMessage("@{0} Please specify the number of the normal request you would like to remove".format(sender))
                        return
                    except ValueError: #if can't get int value
                        await self.sendChatMessage("@{0} Please specify the number of the prio request you would like to remove".format(sender))
                        return
                    try: #try to remove request
                        del self.Requests[0][index - 1]
                        del self.Requests[1][index - 1]
                        await self.updateRequestList("normal")
                        await self.sendChatMessage("Request {0} removed from normal request list".format(index))
                    except IndexError: #if request not in deque
                        await self.sendChatMessage("@{0} Request {1} not in normal request list".format(sender, index))
                else: #if have argument but not valid
                    await self.sendChatMessage("@{0} please specify 'prio' or 'normal'".format(sender))
        elif message[0] == config['Commands']['Prefix'] + config['Commands']['edit-request-command']: #if edit request command
            if await self.permissionCheck("edit-request", userType): #if allowed to run command
                if len(message) == 1: #if request not specified
                    await self.sendChatMessage("@{0} please specify 'prio' or 'normal'".format(sender))
                elif message[1].lower() == "prio": #if specified prio
                    try: #try to get value from chat message
                        index = int(message[2]) #get value from message
                    except IndexError: #if can't get int value
                        await self.sendChatMessage("@{0} Please specify the number of the prio request you would like to remove".format(sender))
                        return
                    except ValueError: #if can't get int value
                        await self.sendChatMessage("@{0} Please specify the number of the prio request you would like to remove".format(sender))
                        return
                    try: #try to edit request
                        if len(message) >= 4: #if request edit specified
                            request = " ".join(message[3:])
                            if len(" ".join(message[1:])) > config.getint('Commands', 'request-character-limit') and config.getint('Commands', 'request-character-limit') != -1:
                                request = request[:config.getint('Commands', 'request-character-limit')]
                        else: #if no request in message
                            await self.sendChatMessage("@{0} Please specify what you would like to edit the request to".format(sender))
                            return
                        self.Requests[2][index - 1] = request
                        await self.updateRequestList("prio")
                        await self.sendChatMessage("Request {0} edited to '{1}' in prio request list".format(index, request))
                    except IndexError: #if request not in deque
                        await self.sendChatMessage("@{0} Request {1} not in prio request list".format(sender, index))
                elif message[1].lower() == "normal": #if specified normal
                    try: #try to get value from chat message
                        index = int(message[2]) #get value from message
                    except IndexError: #if can't get int value
                        await self.sendChatMessage("@{0} Please specify the number of the normal request you would like to remove".format(sender))
                        return
                    except ValueError: #if can't get int value
                        await self.sendChatMessage("@{0} Please specify the number of the prio request you would like to remove".format(sender))
                        return
                    try: #try to edit request
                        if len(message) >= 4: #if request edit specified
                            request = " ".join(message[3:])
                            if len(" ".join(message[1:])) > config.getint('Commands', 'request-character-limit') and config.getint('Commands', 'request-character-limit') != -1:
                                request = request[:config.getint('Commands', 'request-character-limit')]
                        else: #if no request in message
                            await self.sendChatMessage("@{0} Please specify what you would like to edit the request to".format(sender))
                            return
                        self.Requests[0][index - 1] = request
                        await self.updateRequestList("normal")
                        await self.sendChatMessage("Request {0} edited to '{1}' in normal request list".format(index, request))
                    except IndexError: #if request not in deque
                        await self.sendChatMessage("@{0} Request {1} not in normal request list".format(sender, index))
                else: #if have argument but not valid
                    await self.sendChatMessage("@{0} please specify 'prio' or 'normal'".format(sender))
        elif message[0] == config['Commands']['Prefix'] + config['Commands']['reload-config-command']: #if reload config command
            if await self.permissionCheck("reload-config", userType): #if allowed to run command
                config.read("config.ini")
                await self.sendChatMessage("Config reloaded successfully")
        elif message[0] == config['Commands']['Prefix'] + config['Commands']['stop-bot-command']: #if stop bot command
            if await self.permissionCheck("stop-bot", userType): #if allowed to run command
                loop.stop()
        elif message[0] == config['Commands']['Prefix'] + config['Currency']['check-tokens-command']: #if check tokens command
            if config.getboolean("Currency", "enabled"): #if currency enabled
                if len(message) == 1 or not await self.permissionCheck("check-other-users-tokens", userType): #if checking own tokens
                    prioUser = sender
                elif message[1].startswith("@"):
                    prioUser = message[1].lstrip("@")
                else:
                    await self.sendChatMessage("@{0} Please specify a user with @<user>".format(sender))
                    return
                accountStatus, userID = await self.currencyAccountChecking(prioUser, sender, tags["user-id"] if sender == prioUser else None)
                if accountStatus: #if account valid
                    if sender == prioUser: #if sender is user checking tokens
                        await self.sendChatMessage("@{0} You have {1} {2}".format(sender, self.currencyDicts[1][userID], config['Currency']['currency-name']))
                    else: #if user checking other user's tokens
                        await self.sendChatMessage("{0} has {1} {2} @{3}".format(prioUser, self.currencyDicts[1][userID], config['Currency']['currency-name'], sender))
        elif message[0] == config['Commands']['Prefix'] + config['Currency']['token-management-command']: #if manage tokens command
            if await self.permissionCheck("manage-tokens", userType) and config.getboolean("Currency", "enabled"): #if user allowed to run command and currency enabled
                if len(message) == 1 or not message[1].startswith("@") or len(message[1]) == 1: #if second argument not pinging user
                    await self.sendChatMessage("@{0} Please specify user with @<user>".format(sender))
                else: #if correctly pinged user
                    prioUser = message[1].lstrip("@")
                    accountStatus, userID = await self.currencyAccountChecking(prioUser, sender) #check account and create if necessary
                    if accountStatus: #if account valid
                        if len(message) <= 2: #if not specify operation
                            await self.sendChatMessage("@{0} Please specify 'add', 'remove', or 'set'".format(sender))
                        elif message[2] == "add": #if wanting to add tokens
                            try: #try to add tokens
                                self.currencyDicts[1][userID] += int(message[3])
                                await self.sendChatMessage("{0} {1} added to user '{2}'".format(message[3], config['Currency']['currency-name'], prioUser))
                            except ValueError: #if failed (probably due to user not specifying tokens properly)
                                await self.sendChatMessage("@{0} Please specify number of {1}".format((sender), config['Currency']['currency-name']))
                        elif message[2] == "remove": #if wanting to remove tokens
                            try: #try to remove tokens
                                self.currencyDicts[1][userID] -= int(message[3])
                                if self.currencyDicts[1][userID] < 0:
                                    self.currencyDicts[1][userID] = 0
                                await self.sendChatMessage("{0} {1} removed from user '{2}'".format(message[3], config['Currency']['currency-name'], prioUser))
                            except ValueError: #if failed (probably due to user not specifying tokens properly)
                                await self.sendChatMessage("@{0} Please specify number of {1}".format((sender), config['Currency']['currency-name']))
                        elif message[2] == "set": #if wanting to set tokens
                            try: #try to set tokens
                                self.currencyDicts[1][userID] = int(message[3])
                                await self.sendChatMessage("{0} {1} set to '{2}'".format(prioUser, config['Currency']['currency-name'], message[3]))
                            except ValueError: #if failed (probably due to user not specifying tokens properly)
                                await self.sendChatMessage("@{0} Please specify number of {1}".format((sender), config['Currency']['currency-name']))
                        else: #if no proper arguments given
                            await self.sendChatMessage("@{0} Please specify 'add', 'remove', or 'set'".format(sender))
                        with open("currency.json", "w") as currencyData:
                            json.dump(self.currencyDicts, currencyData, indent=2) #rewrite json file to reflect changes
        elif message[0] == config['Commands']['Prefix'] + config['Currency']['clean-accounts-command']: #if clean accounts command
            if await self.permissionCheck("clean-accounts", userType) and config.getboolean("Currency", "enabled"): #if allowed to run command and currency enabled
                for i in self.currencyDicts[1].copy():
                    if self.currencyDicts[1][i] < 1: #if value is 0 or less
                        del self.currencyDicts[1][i] #delete account
                for i in self.currencyDicts[0].copy():
                    if self.currencyDicts[0][i] not in self.currencyDicts[1]: #if userid not in system
                        del self.currencyDicts[0][i] #delete username to user id reference
                await self.sendChatMessage("User accounts cleaned successfully")
                with open("currency.json", "w") as currencyData:
                    json.dump(self.currencyDicts, currencyData, indent=2) #rewrite json file to reflect changes

    async def currencyAccountChecking(self, prioUser, sender, userID=None):
        """Checks whether account for user already exists, and creates one if not"""
        if userID == None: #if no user id provided
            if prioUser.lower() in self.currencyDicts[0]: #if user already in system
                userID = self.currencyDicts[0][prioUser.lower()]
                return True, userID
            elif twitchAPI.session != None: #if api access available
                apiJson = await twitchAPI.twitchAPIGet("/users", {"login":prioUser})
                try:
                    userID = apiJson["data"][0]["id"]
                except IndexError:
                    await self.sendChatMessage("@{0} The user '{1}' does not exist. Please check your spelling.".format(sender, prioUser))
                    return False, None
                except KeyError:
                    await self.sendChatMessage("@{0} The Twitch API is unavailable at this time. Please try again later.".format(sender))
                    return False, None
            else: #if no api access
                await self.sendChatMessage("@{0} Please get @{1} to initialize account by doing '{2}{3}'".format(sender, prioUser, config['Commands']['prefix'], config['Currency']['check-tokens-command']))
                return False, None
        if prioUser.lower() not in self.currencyDicts[0]: #if user not already in dict
            if userID in self.currencyDicts[1]: #if account already exists (name change)
                for i in self.currencyDicts[0].copy():
                    if self.currencyDicts[0][i] == userID: #if value is userID
                        del self.currencyDicts[0][i] #delete username to userid reference
            else: #if account not already exist
                self.currencyDicts[1][userID] = 0 #set tokens to 0
            self.currencyDicts[0][prioUser.lower()] = userID #set username to user id reference
            with open("currency.json", "w") as currencyData:
                json.dump(self.currencyDicts, currencyData, indent=2) #write json file to reflect changes
        return True, userID

    async def currencyAccountManagement(self, userID, operation, value):
        """Performs given operation of given value on given account"""
        if operation == "add":
            self.currencyDicts[1][userID] += value
        elif operation == "subtract":
            if self.currencyDicts[1][userID] >= value:
                self.currencyDicts[1][userID] -= value
            else:
                return False
        elif operation == "set":
            self.currencyDicts[1][userID] = value
            if self.currencyDicts[1][userID] < 0:
                self.currencyDicts[1][userID] = 0
        else:
            return False
        with open("currency.json", "w") as currencyData:
            json.dump(self.currencyDicts, currencyData, indent=2) #write json file to reflect changes
        return True

class TwitchAPI:

    async def twitchAPIConnect(self):
        """Checks if user provided API key and connects to Twitch API if so"""
        if config['Twitch']['api-client-id'] != "": #if api client id provided
            self.session = aiohttp.ClientSession() #start aiohttp session for connecting to twitch api
        else:
            self.session = None #so i can easily check if api access available

    async def twitchAPIGet(self, endpoint, params):
        url = "https://api.twitch.tv/helix" + endpoint #endpoint starts with /
        header = {"Client-ID":config['Twitch']['api-client-id']} #set header with client id for access
        with async_timeout.timeout(10):
            async with self.session.get(url, params=params, headers=header) as r: #get api response
                apiResponseJson = await r.json()
        return apiResponseJson


def newConfig():
    """Generates new config file with default values"""
    print("Making default config value, please supply twitch channel and twitch account in config.ini")
    config["Twitch"] = {'channel':'PUT_CHANNEL_HERE',
                        'username':'PUT_BOT_USERNAME_HERE',
                        'oauth-token':'PUT_OAUTH_TOKEN_HERE',
                        'api-client-id':''}
    config["Commands"] = {'prefix':'!',
                          'request-command':'request',
                          'prio-request-command':'priorequest',
                          'clear-requests-command':'clearrequests',
                          'skip-request-command':'skiprequest',
                          'toggle-requests-command':'togglerequests',
                          'remove-request-command':'removerequest',
                          'edit-request-command':'editrequest',
                          'reload-config-command':'reloadconfig',
                          'stop-bot-command':'stopbot',
                          'request-character-limit':'-1',
                          'max-normal-requests-per-user':'1',
                          'max-prio-requests-per-user':'1',
                          'max-prio-requests':'-1',
                          'max-normal-requests':'-1',
                          'save-requests-for-next-launch':'True'}
    config["Currency"] = {'enabled':'True',
                          'subs-auto-give-tokens':'True',
                          'sub-prime-tokens':'2',
                          'sub-tier1-tokens':'2',
                          'sub-tier2-tokens':'5',
                          'sub-tier3-tokens':'12',
                          'sub-gift-tokens':'receiver',
                          'sub-gift-tier1-receiver-split':'50',
                          'sub-gift-tier2-receiver-split':'50',
                          'sub-gift-tier3-receiver-split':'50',
                          'bits-auto-give-tokens':'True',
                          'bits-per-token':'100',
                          'currency-name':'tokens',
                          'check-tokens-command':'tokens',
                          'token-management-command':'managetokens',
                          'clean-accounts-command':'cleanaccounts'}
    config["Permissions"] = {'request':'everyone',
                             'prio-request-no-currency':'broadcaster,mod',
                             'clear-requests':'broadcaster,mod',
                             'skip-request':'broadcaster,mod',
                             'toggle-requests':'broadcaster,mod',
                             'remove-request':'broadcaster,mod',
                             'edit-request':'broadcaster,mod',
                             'reload-config':'broadcaster',
                             'stop-bot':'broadcaster',
                             'manage-tokens':'broadcaster,mod',
                             'check-other-users-tokens':'broadcaster,mod',
                             'clean-accounts':'broadcaster'}
    config["Text"] = {'normal-request-text':'Normal Requests',
                      'prio-request-text':'Priority Requests',
                      'write-limit-reached-to-file':'True',
                      'write-request-status-to-file':'True'}
    with open("config.ini", "w") as configfile:
        config.write(configfile)

def checkFiles():
    """Checks if config file exists and is correct and overwrites requests files"""
    if not os.path.exists("config.ini"):
        newConfig()
        return False
    else:
        config.read("config.ini")
        if not "Twitch" in config.sections() or not "Commands" in config.sections() or not "Currency" in config.sections() or not "Permissions" in config.sections() or not "Text" in config.sections():
            print("Invalid config, regenerating")
            newConfig()
            return False
        return True

config = configparser.ConfigParser()
if checkFiles():
    twitchBot = TwitchListener()
    twitchAPI = TwitchAPI()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(twitchBot.twitchConnect('irc.chat.twitch.tv', loop))
    loop.run_until_complete(twitchAPI.twitchAPIConnect())
    loop.run_until_complete(twitchBot.updateRequestList())
    loop.create_task(twitchBot.ircListener(loop))
    try:
        loop.run_forever()
    finally:
        loop.close()
else:
    print("Exiting")
