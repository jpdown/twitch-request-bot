# Twitch Song Request Bot
Twitch song request bot, mainly for rhythm game streamers. Saves requests to text file currently. Credits to iamquinnsane for the icon

## Commands
* !request - Normal request command
* !priorequest - Priority request
* !clearrequests - Clears given request request list \[normal/prio/<none> for both]
* !skiprequest - Skips request based on given argument \[normal/prio]
* !removerequest - Removes request from given list \[normal/prio]
* !editrequest - Edits request from given list \[normal/prio]
* !togglerequests - Toggles requests on and off \[normal/prio]
* !reloadconfig - Reloads config file
* !stopbot - Stops bot
* !tokens - Checks your own tokens or tokens of given user
* !managetokens - Token management command \[add/remove/set]
* !cleanaccounts - Removes any accounts from system that have 0 tokens

## Configuration
All commands and prefix are in the config file and can be changed as needed. The other config options are
#### \[Twitch]
* channel - Put twitch channel here, omit the #
* username - Put bot's username here
* oauth-token - Put bot's oauth token here, retrieve from [here](https://twitchapps.com/tmi/)
* api-client-id - For Twitch API access, required to use !managetokens on a user who has not run !tokens or initialized their account in some other way. [Register a new bot application](https://dev.twitch.tv/dashboard/apps), putting `http://localhost` for the OAuth Redirect URI and paste the client id here.
#### \[Commands]
* max-normal-requests-per-user - Max requests a user can do using !request. -1 for unlimited
* max-prio-requests-per-user - Max priority requests a user can do using !priorequest. -1 for unlimited
* max-prio-requests - Max priority requests you'll accept in general, -1 for unlimited
* max-normal-requests - Max normal requests you'll accept in general, -1 for unlimited
* save-requests-for-next-launch - Whether to save requests to load on next launch
* request-character-limit - Character limit for requests, removes any excess characters
#### \[Currency]
* enabled - Global toggle for the currency system
* subs-auto-give-tokens - Whether to automatically grant tokens for subscriptions
* sub-prime-tokens and the other tiers - How many tokens to grant per sub tier
* sub-gift-tokens - "receiver/gifter/split" who to give the tokens too, see next line for details on split
* sub-gift-tier1-receiver-split and other tiers - Percentage of tokens to give to the receiver, the rest will go to the gifter
* bits-auto-give-tokens - Whether to automatically grant tokens for bits
* bits-per-token - How many bits to get 1 token, rounded down
* currency-name - Changes name of currency
#### \[Permissions]
A comma separated list of who you want to be able to use a command. Options are `broadcaster`,`mod`,`sub`,`everyone`
#### \[Text]
* normal-request-text - Changes the text "Normal requests:" in normalrequests.txt
* prio-request-text - Changes the text "Priority requests:" in priorequests.txt 
* write-limit-reached-to-file - Whether to write (LIMIT REACHED) to the requests text file
* write-request-status-to-file - Whether to write (OPEN) or (CLOSED) to the requests text file

## Install
#### Windows
Download `twitch-request-bot.exe` from [releases](https://github.com/PoisonedPanther/twitch-request-bot/releases), run
#### Windows/Mac/Linux
`pip install aiohttp async_timeout`
Run `twitch-request-bot.py` with Python 3

## How to Use
On first run `config.ini` will be generated. Modify the `config.ini` as needed, making sure to give a Twitch channel and bot account details. `normalrequests.txt` `priorequests.txt` and `combinedrequests.txt` will be generated and updated as request list changes. Errors on close are normal and do not affect anything.

## Support
Please join the [Discord server](http://discord.gg/pkVwdqT) for support and update announcements
